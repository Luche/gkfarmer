﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginsCommon;
using Plugins;
using System.Threading;
using System.Windows.Forms;

namespace GKFarmer
{
    public class Main : IPlugin
    {
        public static Thread UIThread;
        public static UI ui;
        public static uint TargetId = 0;
        public static Vector3 Location = new Vector3();
        public static string CombatProfile = "";
        public static bool PluginRunning = false;
        // Tigerius - 43732
        // Cleo - 43733
        public string Author
        {
            get
            {
                return "Liz";
            }
        }

        public string Description
        {
            get
            {
                return "Plugin for farming GK points in Gaia Light by exploiting the 2 tigers bug";
            }
        }

        public string Name
        {
            get
            {
                return "GKFarmer";
            }
        }

        public Version Version
        {
            get
            {
                return new Version(1, 0, 0);
            }
        }

        public void OnButtonClick()
        {
            if (!Skandia.IsInGame)
                return;
            if (ui == null)
            {
                StartUIThread();
                return;
            }
            else if (ui.Visible)
            {
                if (ui.InvokeRequired)
                {
                    ui.Invoke((MethodInvoker)delegate { ui.Hide(); });
                }
                else
                    ui.Hide();
            }
            else
            {
                if (ui.InvokeRequired)
                {
                    ui.Invoke((MethodInvoker)delegate { ui.Show(); });
                }
                else
                {
                    ui.Show();
                }
            }
        }
        public static bool StartUIThread()
        {
            if (UIThread == null ||
                UIThread.ThreadState == System.Threading.ThreadState.Aborted ||
                UIThread.ThreadState == System.Threading.ThreadState.Stopped ||
                UIThread.ThreadState == System.Threading.ThreadState.Unstarted)
            {
                UIThread = new Thread(UIThreadStartMethod);
                UIThread.SetApartmentState(ApartmentState.STA);
                UIThread.Start();
                return true;
            }
            else
                return false;
        }
        private static void UIThreadStartMethod()
        {
            if (ui == null)
            {
                ui = new UI();
            }
            ui.ShowDialog();
        }

        public void OnStart()
        {
        }

        public void OnStop(bool off)
        {
            try
            {
                if (ui != null && ui.InvokeRequired)
                {
                    ui.Invoke((MethodInvoker)delegate
                    {
                        ui.Close();
                        ui.Dispose();
                    });
                }
                else
                {
                    if (ui != null && !ui.IsDisposed)
                    {
                        ui.Close();
                        ui.Dispose();
                    }
                }
            }
            catch { }
        }

        public void Pulse()
        {
            Skandia.Update();
            if (!Skandia.IsInGame || !PluginRunning)
                return;
            if (TargetId == 0)
            {
                Skandia.MessageLog("No proper target selected");
                return;
            }
            if (Location == new Vector3())
            {
                Skandia.MessageLog("No proper location set");
                return;
            }
            if (Skandia.Me.Location3D.Distance(Location) > 5)
            {
                Skandia.Core.Mover.MoveAt(Location, -1, 2);
                if (Skandia.Core.GetAutoBuffState())
                    Skandia.Core.ToggleAutoBuffBot(false);
            }
            if (Skandia.Me.IsInParty && Skandia.Me.PartyMembers[0].IsDead)
            {
                PluginRunning = false;
                return;
            }
            var target = ObjectManager.ObjectList.FirstOrDefault(x => x.IsValid && x.Template != null && x.Template.Id == TargetId && x.Info.IsAlive);
            if (target == null)
            {
                if (Skandia.Core.GetAutoBuffState())
                    Skandia.Core.ToggleAutoBuffBot(false);
                Skandia.Me.SetTarget(Skandia.Me.Guid);
                return;
            }
            if (Skandia.Me.CanSendSkill(62046))
                Skandia.Me.SendSkill(62046);

            Skandia.Me.SetTarget(target.Guid);
            if (!Skandia.Core.GetAutoBuffState())
                Skandia.Core.ToggleAutoBuffBot(true);
        }
    }
}
