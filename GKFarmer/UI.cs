﻿using Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GKFarmer
{
    public partial class UI : Form
    {
        public UI()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            updateStartStop();
        }

        public void updateStartStop()
        {
            Main.PluginRunning = !Main.PluginRunning;
            if (!Main.PluginRunning)
                Skandia.Core.ToggleAutoBuffBot(false);
            button2.Text = button2.Text == "Start" ? "Stop" : "Start";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Lock target
            if (Skandia.Me.GotTarget)
            {
                Main.TargetId = Skandia.Me.CurrentTarget.Template.Id;
                label2.Text = Skandia.Me.CurrentTarget.Name;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Lock location
            var l = Skandia.Me.Location3D;
            Main.Location = l;
            label3.Text = "X:" + Math.Round(l.X) + ", Y:" + Math.Round(l.Y) + ", Z:" + Math.Round(l.Z);
        }
    }
}
