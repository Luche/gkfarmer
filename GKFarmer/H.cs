﻿using Plugins;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace GKFarmer
{
    public static class H
    {
        private static XmlSerializer serializer;
        public static T DeserializeFromFile<T>(string file, bool useCutomDirectory = false)
        {
            if (!file.EndsWith(".xml"))
                file = file + ".xml";

            try
            {
                serializer = new XmlSerializer(typeof(T));
                if (useCutomDirectory)
                    using (var reader = new StreamReader(file))
                        return (T)serializer.Deserialize(reader);
                else
                    using (var reader = new StreamReader(Path.Combine(ProfilesDirectory, file)))
                        return (T)serializer.Deserialize(reader);
            }
            catch
            {
                MessageBox.Show("Your profile.xml is corrupted. Please delete it and let the plugin generate a new one for you.");
                return default(T);
            }
        }

        public static void SerializeToFile<T>(string fileName, T instance, bool useCustomDirectory = false)
        {
            if (!fileName.EndsWith(".xml"))
                fileName = fileName + ".xml";
            if (!Directory.Exists(ProfilesDirectory))
                Directory.CreateDirectory(ProfilesDirectory);

            serializer = new XmlSerializer(typeof(T));
            if (useCustomDirectory)
                using (var writer = new StreamWriter(fileName))
                    serializer.Serialize(writer, instance);
            else
                using (var writer = new StreamWriter(Path.Combine(ProfilesDirectory, fileName)))
                    serializer.Serialize(writer, instance);
        }

        public static string SerializeToString<T>(T instance)
        {
            serializer = new XmlSerializer(typeof(T));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, instance);
                return writer.ToString();
            }
        }

        public static T DeserializeFromString<T>(string data)
        {
            try
            {
                serializer = new XmlSerializer(typeof(T));
                using (var reader = new StringReader(data))
                    return (T)serializer.Deserialize(reader);
            }
            catch
            {
                MessageBox.Show("Internal XML data is corrupted. Please report the the developer.");
                return default(T);
            }
        }
        public static string ProfilesDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "plugins", "GKFarmer");
            }
        }
        public static string CombatProfilesDir
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.Combine(Directory.GetParent(Path.GetDirectoryName(path)).FullName, "profiles", "combat");
            }
        }

        public static List<string> CombatProfiles()
        {
            List<string> filePaths = Directory.GetFiles(CombatProfilesDir, "*.akcs").ToList();
            List<string> files = new List<string>();
            foreach (var file in filePaths)
            {
                files.Add(Path.GetFileNameWithoutExtension(file));
            }
            return files;
        }
    }
}
